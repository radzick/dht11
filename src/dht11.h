/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   dht11.h
 * Author: radzick
 *
 * Created on 16 listopada 2017, 17:06
 */

#ifndef DHT11_H
#define DHT11_H

#ifdef __cplusplus
extern "C" {
#endif

#define WIRINGPI
    
    
// If raspberry pi us unused define your 
#if defined (WIRINGPI)
#include <wiringPi.h>
#include <lcd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//USE WIRINGPI PIN NUMBERS
#define LCD_RS  25               //Register select pin
#define LCD_E   24               //Enable Pin
#define LCD_D4  23               //Data pin 4
#define LCD_D5  22               //Data pin 5
#define LCD_D6  21               //Data pin 6
#define LCD_D7  14               //Data pin 7
#define MAXTIMINGS 85
#define DHTPIN 7
    
#define thisSetup   wiringPiSetup
#define thisPrintf  printf

#elif defined (STM32)
    
#define LOW
#define	HIGH
#define	INPUT
#define	OUTPUT

#define LCD_RS 
#define LCD_E 
#define LCD_D4
#define LCD_D5
#define LCD_D6 
#define LCD_D7 
#define MAXTIMINGS
#define DHTPIN
    
#define setup()
#define printf (const char *__restrict __format, ...)
#define pinMode(int pin, int mode) 
#define digitalWrite(int pin, int level)
#define digitalRead(int pin)
#define delay(uint32_t timeout)
#define delayMicroseconds(uint32_t timeout)

#define lcdHome        (const int fd) 
#define lcdClear       (const int fd) 
#define lcdDisplay     (const int fd, int state) 
#define lcdCursor      (const int fd, int state) 
#define lcdCursorBlink (const int fd, int state) 
#define lcdSendCommand (const int fd, unsigned char command) 
#define lcdPosition    (const int fd, int x, int y) 
#define lcdCharDef     (const int fd, int index, unsigned char data [8]) 
#define lcdPutchar     (const int fd, unsigned char data) 
#define lcdPuts        (const int fd, const char *string) 
#define lcdPrintf      (const int fd, const char *message, ...) 
#define lcdInit (const int rows, const int cols, const int bits,    \
	const int rs, const int strb,                               \
	const int d0, const int d1, const int d2, const int d3,     \
        const int d4, const int d5, const int d6, const int d7)   
    
#else
// other port
#endif

    
#ifdef __cplusplus
}
#endif

#endif /* DHT11_H */

