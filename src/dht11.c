#include "dht11.h"

//CUSTOM DEFINES
#define TIMEOUT 1000    //1000us = 1ms

int lcd;

uint32_t countExpectedPulse(int level)
{
    uint32_t count = 0;
    while (digitalRead(DHTPIN) == level)
    {
        if (count++ >= TIMEOUT) 
        {
            return 0; // exceeded timeout, fail
        }
        delayMicroseconds(1);
    }
    return count;
}

void read_dht11_dat()
{
    uint8_t i;
    uint32_t cycles[80];
    uint8_t data[5];
    
    /*
     * Description of one-wire data transfer from DHT11
     * 
     * VCC ____              ______            ______       __       ____
     *         \            /     |\          /     |\     /  \     /    \
     * GND      \____//____/      | \________/      | \___/    \___/      \ 
     *                            |                 |
     *            > 18ms  ->  40us  -> 80us -> 80us -> 50+28us -> 50+70us 
     * >---MCU-------------------->---DHT11--------->--- "0" -->-- "1" --->
     * 
     * Data format:
     * 8bit integral RH data + 8bit decimal RH data + 8bit integral T data + 
     * 8bit decimal T data + 8bit check sum. If the data transmission right,
     * the check-sum should be the last 8bit of "8bit integral RH data + 
     * 8bit decimal RH data + 8bit integral T data + 8bit decimal T data".
     * 
     */

    /* MCU wakes up the DHT11*/
    pinMode(DHTPIN, OUTPUT);    // initially set as output to reset
    digitalWrite(DHTPIN, LOW);  // signal to wakeup the sensor
    delay(20);                  // wait at least 18ms for the wakeup
    digitalWrite(DHTPIN, HIGH); // release the bus
    delayMicroseconds(40);      // keep as output for 40us 
    pinMode(DHTPIN, INPUT);     // set pin as input and probe

    /* Init sequence from the DHT11 */
    if(!countExpectedPulse(LOW))
    {
        thisPrintf("Wait for start signal low pulse from DHT11.");
    }
    if(!countExpectedPulse(HIGH))
    {
        thisPrintf("Wait for start signal high pulse from DHT11.");
    }

    /* Reset 40 bits of received data to zero. */
    data[0] = data[1] = data[2] = data[3] = data[4] = 0;
    
    for(i = 0; i < 80; i += 2)
    {
        cycles[i]   = countExpectedPulse(LOW);  /* LOW */
        cycles[i+1] = countExpectedPulse(HIGH); /* HIGH */
    }
    
    /*
     * Inspect pulses and determine which ones are 0 (high state cycle count < low
     * state cycle count), or 1 (high state cycle count > low state cycle count).
     */
    for(i = 0; i < 40; ++i)
    {
        uint32_t lowCycles  = cycles[2*i];
        uint32_t highCycles = cycles[2*i+1];
        if((lowCycles == 0) || (highCycles == 0))
        {
            thisPrintf("Timeout waiting for pulse.\n");
            continue;
        }
        data[i/8] <<= 1;
        
        /* Now compare the low and high cycle times to see if the bit is a 0 or 1. */
        if(highCycles > lowCycles)
        {
            /* High cycles are greater than 50us low cycle count, must be a 1. */
            data[i/8] |= 1;
        }
        /*
         * Else high cycles are less than (or equal to, a weird case) the 50us low
         * cycle count so this must be a zero.  Nothing needs to be changed in the
         * stored data.
         */
    }

    thisPrintf("Received data: [%d][%d][%d][%d][%d]\n",data[0],data[1],data[2],data[3],data[4]);
    thisPrintf("Humidity: %d.%d %%, t : %d\r\n", data[0], data[2]);
    lcdPosition(lcd, 0, 0);
    lcdPrintf(lcd, "Humidity: %d.%d %%\n", data[0], data[1]);
    lcdPosition(lcd, 0, 1);
    lcdPrintf(lcd, "Temp: %d.0 C", data[2]);
    
    /* Check we read 40 bits and that the checksum matches. */
    if(data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))
    {
        /* Checksum is fine */
    }
    else
    {
        thisPrintf("Checksum failure!\n");
    }
}

int main(int argc, char **argv)
{
    thisSetup();
    lcd = lcdInit (2, 16, 4, LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 0, 0, 0, 0);

    thisPrintf("dht11 example\n");
    lcdPrintf(lcd, "dht11 example");

    while (1)
    {
            delay(1000); // wait for the next sample
            read_dht11_dat();
    }

    return (0);
}
